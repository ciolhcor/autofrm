all: doc test
force: touch all
touch:
	touch 2-Contents/
doc:
	(cd 6-Documentations/;make)
test:
	(cd 3-Tests/;make)
