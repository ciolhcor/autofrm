<?php

if(!defined('AUTOFRM_LIB')) define('AUTOFRM_LIB', '../2-Contents/lib/');
if(!defined('AUTOFRM_CONF')) define('AUTOFRM_CONF', 'config/');
if(!defined('PHPUNIT_EXEC_FULL_LOAD_CLASS')) define('PHPUNIT_EXEC_FULL_LOAD_CLASS', true);

require_once '../2-Contents/lib/bootstrap.inc.php';

