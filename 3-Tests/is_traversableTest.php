<?php

class is_traversableTest_traversable implements IteratorAggregate {
  public function getIterator() { return new ArrayIterator(['a', 'b']); }
}
class is_traversableTest_nottraversable {
  public function getIterator() { return new ArrayIterator(['a', 'b']); }
}

class is_traversableTest extends PHPUnit_Framework_TestCase
{
  public function testScalarInt()
  {
    $this->assertFalse(is_traversable(10));
    $this->assertFalse(is_traversable(-10));
  }
  public function testScalarString()
  {
    $this->assertFalse(is_traversable("asdf"));
  }
  public function testScalarArray()
  {
    $this->assertTrue(is_traversable(["asdf"]));
  }
  public function testobjectIteratorAggregate()
  {
    $this->assertTrue(is_traversable(new is_traversableTest_traversable));
  }
  public function testobject()
  {
    $this->assertFalse(is_traversable(new is_traversableTest_nottraversable));
  }
}
