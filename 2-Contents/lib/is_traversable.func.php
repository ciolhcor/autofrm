<?php

/**
 * @brief Test if $obj is travesable
 * @details Test if $obj is an instance of Traversable of is an array
 * @param[in] mixed $obj The variable for test.
 * @retval true If $obj is Traversable
 * @retval false Else
 * @author Ciol Hcor
 * @date 2015-04-08
 */
function is_traversable ($obj) {
  return (is_array($obj) || $obj instanceof Traversable);
}
