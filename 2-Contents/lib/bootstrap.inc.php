<?php

// implement l'autogargement des classes
spl_autoload_register(function ($class) {
  $class = str_replace('\\', '/', $class);
  $class = strtolower($class);
  $file = AUTOFRM_LIB.$class.'.class.php';
  if(is_file($file)) {
    require_once($file);
  }
});
// autoload of interface
spl_autoload_register(function ($interface) {
  $interface = str_replace('\\', '/', $interface);
  $interface = strtolower($interface);
  $file = AUTOFRM_LIB.$interface.'.interface.php';
  if(is_file($file)) {
    require_once($file);
  }
});

if(defined('PHPUNIT_EXEC_FULL_LOAD_CLASS') && PHPUNIT_EXEC_FULL_LOAD_CLASS) {
  foreach(scandir(AUTOFRM_LIB, SCANDIR_SORT_NONE) as $filename) {
    $file = AUTOFRM_LIB.$filename;
    if($filename === '.' || $filename === '..') continue;
    if(!is_file($file)) continue;
    if(!is_readable($file)) continue;
    if(preg_match('~^\.~', $filename)) continue;
    if(!preg_match('~\.class\.php$~', $filename)) continue;
    require_once $file;
  }
}

foreach(scandir(AUTOFRM_LIB, SCANDIR_SORT_NONE) as $filename) {
  $file = AUTOFRM_LIB.$filename;
  if($filename === '.' || $filename === '..') continue;
  if(!is_file($file)) continue;
  if(!is_readable($file)) continue;
  if(preg_match('~^\.~', $filename)) continue;
  if(!preg_match('~\.func\.php$~', $filename)) continue;
  require_once $file;
}
