<?php

/**
 * @brief Interface with a database
 * @details Interface with a database from any driver module in php,
 * The condition is the driver is obligatory an object and all error
 * throw an exception.
 */
class db
{
  //! Object for configuration
  private $config=null;

  //! Object for access to database
  private $obj_sql=null;
  
  /**
   * @brief Create a new db class.
   * @details Create and configure with default parameter
   * from AUTOFRM_CONF/autofrm.ini
   * @retval db A object db
   * @author Ciol Hcor
   * @date 2015-04-08
   */
  function __construct ()
  {
    // get the configuration
    $this->config = new config();
    $this->config = $this->config->database;

    // get name of class for interaction with DB
    $refl_sql = new ReflectionClass('hcor\\'.$this->config->module);
    
//    if($refl_sql->implementsInterface(

    var_dump($refl_sql);
  }
}
