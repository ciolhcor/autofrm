<?php

class config implements IteratorAggregate
{
  //! All file loaded for not reload from disk
  static private $loaded = [];

  //! The link to configuration for instance
  private $conf = [];

  //! The name unsed in constructor
  private $name = null;
  
  /**
   * The constructor of config class
   * @author Ciol Hcor
   * @date 2015-04-07
   */
  public function __construct($name=null)
  {
    $this->name = $name;
    if($name === null && is_file(AUTOFRM_CONF.'autofrm.ini'))
      $filename = AUTOFRM_CONF.'autofrm.ini';
    elseif(is_resource($name) && 'stream' === get_resource_type($name))
      $filename = $name;
    elseif(preg_match('~^[a-zA-Z]+$~', $name) && is_file(AUTOFRM_CONF.$name.'.ini'))
      $filename = AUTOFRM_CONF.$name.'.ini';
    elseif(is_file(AUTOFRM_CONF.'autofrm.ini'))
      $filename = AUTOFRM_CONF.'autofrm.ini';
    else
      throw new Exception('file ('.$name.') don\'t exist',1);

    if(!isset(self::$loaded[$filename]))
    {
      if(is_resource($filename) && 'stream' === get_resource_type($filename))
        self::$loaded[$filename] = parse_ini_string(stream_get_contents($filename), true, INI_SCANNER_NORMAL);
      else self::$loaded[$filename] = parse_ini_file($filename, true, INI_SCANNER_NORMAL);
    }
    $this->conf =& self::$loaded[$filename];
  }

  /**
   * @brief This function return the subconfigation or the value of configuration
   * @details If the parameter $name point in category of configuration the return
   * is a new object for this category, else if is only a variable of configuration
   * the value of configuration is returned.
   * @param[in] string $name The name of parameter for access
   * @retval config If $name is a category
   * @retval scalar If $name isn't category
   * @retval null If $name doesn't exist
   * @author Ciol Hcor
   * @date 2015-04-07
   */
  public function __get($name) {
    if(isset($this->conf[$name])) {
      if(is_array($this->conf[$name])) {
        $new = new config($this->name);
        $new->conf =& $this->conf[$name];
        return $new;
      } else {
        return $this->conf[$name];
      }
    }
    return null;
  }

  public function __isset($name) {
    return isset($this->conf[$name]);
  }

  public function getIterator() {
    return new RecursiveArrayIterator($this->conf);
  }
}
